<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('category','CategoryController@index')->name('category.index');
Route::get('category/subcategory','CategoryController@create')->name('subcategory.create');
Route::get('/category/subcategory/create/template/{id}','CategoryController@createtemplate')->name('createtemplate.createtemplate');
Route::POST('/category/subcategory/create/template/store','CategoryController@store')->name('createtemplate.store');


Route::get('/subcategory/edit','CategoryController@editTemplate')->name('createtemplate.edit');

Route::post('/upload_image', function() {
    $CKEditor = Input::get('CKEditor');
    $funcNum = Input::get('CKEditorFuncNum');
    $message = $url = '';
    if (Input::hasFile('upload')) {
        $file = Input::file('upload');
        if ($file->isValid()) {
            $filename = $file->getClientOriginalName();
            $file->move(storage_path().'/images/', $filename);
            $url = public_path() .'/images/' . $filename;
        } else {
            $message = 'An error occured while uploading the file.';
        }
    } else {
        $message = 'No file uploaded.';
    }
    return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
});

Route::get('subcategory/contact_detail','CategoryController@details')->name('employee.details');


Route::POST('/category/subcategory/create/template/send/mail','CategoryController@sendmail')->name('createtemplate.sendmail');

Route::get('/test','CategoryController@testEmail')->name('test.dd');

Route::post('/send-mail','CategoryController@sendEmail')->name('send.mail');

Route::post('/set-option','CategoryController@setOption')->name('set.option');
Route::post('/sendemail-latter','CategoryController@sendMailLatter')->name('sendmail.latter');
Route::get('/model', function () {
    return view('modal');
});