<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  	
  	p {
    margin: 0 0 10px;
    text-align: center;
}

.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
}
  </style>
</head>
<body>
 
<div class="container">
  <h2>SubCategory Campaign</h2>
  <div class="panel panel-default">
    <div class="panel-heading">Select SubCategory...</div>
    <div class="panel-body">
 <form method="POST" action="">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
      @foreach ($query as $key => $data)	   
      <div class="col-md-3 col-sm-6 col-xs-12">
    	<div class="card ">
          <div class="cardImg">
           <a href="<?=route('createtemplate.edit', ['id' => $data['id']])?>"> <img class="center" src="/subcategoryimage/{{$data['image']}}" height="150px" weight="150px" style=""> </a>
           <!--  <div class="imageTag"> <span class="icon icon-Eye mr-10"></span> 824 </div> -->
            <div class="imageLocation">
                <p><span class="icon icon-Pointer"></span>{{$data['subcate_name']}}</p>
              <!-- <p><span class="icon icon-Tshirt"></span>Fashion</p>  -->
            </div>
         </div>         
        </div>
      </div>
      @endforeach
 </form>
     </div>
    </div>
  </div>
</div>
</body>
</html>
<!-- <?=route('createtemplate.edit', ['id' => $data['id']])?> -->