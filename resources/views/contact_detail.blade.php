<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">
      <meta name="apple-mobile-web-app-title" content="Weather PWA">
      <meta name="msapplication-TileImage" content="images/icons/icon-144x144.png">
      <meta name="msapplication-TileColor" content="#2F3BA2">
      
      <link rel="apple-touch-icon" href="images/icons/icon-152x152.png">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="/css/style.css">
      <link href="/css/style.min.css" type="text/css" rel="stylesheet">
      <link href="/toastr-master/toastr.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
      <link rel="manifest" href="./manifest.json">
      <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">      
      <link rel="stylesheet" href="/css/style.css">
      <link rel="stylesheet" type="text/css" href="../css/daterangepicker.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css">
      <style>
         /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
         *, *:before, *:after {
         box-sizing: border-box;
         }
         html {
         width: 100%;
         height: 100%;
         position: relative;
         background: #222 url(https://i.cloudup.com/B-nGjfTv8k.jpg) no-repeat;
         background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(https://i.cloudup.com/B-nGjfTv8k.jpg);
         background-size: cover;
         }
         .options {
         position: absolute;
         left: 0;
         right: 0;
         top: 50%;
         transform: translateY(-50%);
         color: #ddd;
         padding: 48px;
         font-family: 'Open Sans', sans-serif;
         text-align: center;
         }
         .options input {
         display: none;
         }
         .options > label {
         display: inline-block;
         position: relative;
         margin-right: 16px;
         padding-left: 24px;
         cursor: pointer;
         }
         .options > label:before {
         content: "";
         display: block;
         position: absolute;
         width: 16px;
         height: 16px;
         left: 0;
         top: 50%;
         margin-top: -8px;
         border: 1px solid #fff;
         border-radius: 8px;
         }
         .options > label:after {
         content: "";
         display: block;
         position: absolute;
         width: 0;
         height: 0;
         top: 50%;
         left: 8px;
         margin-top: 0;
         background: #fff;
         border-radius: 4px;
         transition: .2s ease-in-out;
         }
         .options :checked + label:after {
         height: 8px;
         width: 8px;
         margin-top: -4px;
         left: 4px;
         }
      </style>
      
      <!-- <link rel="stylesheet" type="text/css" media="screen"
         href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"> -->
      
      
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> -->
   </head>
   <body class="light_bg">
      <!--  <form method="POST" action="{{ route('createtemplate.sendmail') }}" enctype="multipart/form-data"> -->
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="contents" value="{{ $content }}" id="template_content">
      <div id="main_wrapper" class="landscape_show">
         <!--Menu open Code Starts -->
         <!--Search open starts-->
         <!--Search open Ends-->
         <!--notification starts-->
         <section class="list_contributors_wrap padding_bottom">
            <div class="responsive_container">
               <div class="list_contribute_header">
                  <h4 class="floatL"></h4>
                  <!-- <div class="list_contri floatR">
                     <input type="text" name="search" placeholder="Search" >
                     </div> -->
                  </br></br></br></br>
                  </br>
                  <div class="clearfix"></div>
               </div>
               <!-- <div class="row col-md-12">
                  <div class="col-md-6">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input id="timepicker1" type="text" class="form-control" style="height: 40px;">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                    
                  </div>
                  <div class="row col-md-6"></div>
              </div> -->
               <div class="clearfix"></div>
               <table class="table" id="email_table">
                    <thead>
                        <tr>
                            <th><input class="styled_checkbox all_check" id="contributorname" type="checkbox" >
                              <label for="contributorname">Select All</label></th>
                            <th>Email</th>
                            <th>Number</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($detail as $key => $data)
                      <tr>
                        <td><input type="hidden" id="contact_id" value="<?=$data['id']?>">
                           <input class="styled_checkbox single_check" id="name_<?=$data['id']?>" type="checkbox" value="<?=$data['id']?>"><label for="name_<?=$data['id']?>">{{$data['name']}}</label>
                         </td>
                         <td>{{$data['email_id']}}</td>
                        <td>{{$data['number']}}</td>
                      </tr>
                    @endforeach       
                    </tbody>
                </table>
               <!-- <div class="contributors_list_table custom_scrollbar">
                  <div class="header_list">
                     <ul>
                        <li>
                           <input class="styled_checkbox all_check" id="contributorname" type="checkbox" >
                           <label for="contributorname">Select All</label>
                        </li>
                        <li>Email</li>
                        <li>Number</li>
                     </ul>
                     <div class="clearfix"></div>
                  </div> -->
                 <!--  <div class="content_list border_bottom">
                     <ul>
                        <li>
                           <input type="hidden" id="contact_id" value="<?=$data['id']?>">
                           <input class="styled_checkbox single_check" id="name_<?=$data['id']?>" type="checkbox" value="<?=$data['id']?>">
                           <label for="name_<?=$data['id']?>">{{$data['name']}}</label>
                        </li>
                        <li>{{$data['email_id']}}</li>
                        <li>{{$data['number']}}</li>
                     </ul>
                     <div class="clearfix"></div>
                  </div> -->
               <!-- </div> -->
               <!-- <div class="container">
                <div class="input-group bootstrap-timepicker timepicker">
                    <input id="timepicker1" type="text" class="form-control" style="height: 40px;">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                </div>
              </div> -->
               
               <div class="clearfix_right"></div>
               <div class="row col-md-12"  >
                  <div class="col-md-6" >
                    <div style="display: none;" id="date_time">
                    <div class="col-md-6">
                      <div class="input-group bootstrap-timepicker timepicker">
                          <input id="timepicker1" type="text" class="form-control" style="height: 40px;">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>                      
                    </div>
                    <div class="col-md-6">
                      <div class="input-group bootstrap-timepicker timepicker">
                        <input class="form-control" type="text" id="datepicker" name="datepicker" style="height: 40px; width: 297px;" readonly='true'>
                      </div>                      
                    </div> 
                    </div>                   
                  </div>
                  <div class="row col-md-3"></div>
                  <div class="row col-md-3">
                   <button id="latter" class="btn btn-primary pull-right" value='latter'>Latter</button>
                    <button id="send_email" class="btn btn-primary pull-right send_email" value="now">Now</button>
                  </div>
              </div>
               
               <!-- <button id="send_email" class="btn btn-primary pull-right">Send Mail</button>  -->
            </div>
         </section>
      </div>
      </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
      <script type="text/javascript" src="/js/app.min.js"></script>
      <script  src="/toastr-master/toastr.min.js"></script>
      
      <script type="text/javascript" src="/js/jquery.form.min.js"></script>
      
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
      <script type="text/javascript" src="/js/daterangepicker.min.js"></script>
      <script>
         $(document).ready(function(){
            $('.enter_record').hide();
            $('.show_div').hide();
            $("#edit").click(function(){
               var message = $('textarea#messageArea').val();
               var data = $('#message_data').html(message);
            });
          
             $('input[name="datepicker"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
              });
        });          
          
         $(document).ready(function(){
            $(".single_check").click(function () {
              var user_id = $(this).val();
            });
         });
         
         $(document).ready(function(){   
              $('#latter').click(function(){
                $('#date_time').css('display','block');
                $('#send_email').css('display','none');
                $('#latter').addClass("send_email");
                $('#send_email').removeClass("send_email");
                $('#latter').html("SEND");
              });
                
              // });
              // send now                    
             // $('.send_email').on('click',function(){
              $('body').on('click',".send_email",function(){
                console.log('hi');
                var btn = $(this).val();
                var time = $('#timepicker1').val();
                console.log(time);
                var total_user = $('input:checkbox.single_check:checked').length;
                // var radioValue = $("input[name='mail_type']:checked").val();
                var id = "<?= $id ?>";
                var selectedVal = new Array();
                $('input:checkbox.single_check').each(function () {
                       if($(this).is(':checked'))
                       {                         
                           selectedVal.push(this.value);       
                       }
                   });
              
                if($('input:checkbox.single_check').is(':checked')){
                      
                      var selected_id = selectedVal;                          
                      var token = "<?=csrf_token()?>";  
                      $.ajax({
                        url:"<?=route('set.option');?>",
                        type:'post',
                        dataType:'json',
                        data: {_token:token,selected_id:selected_id,id:id,btn_val:btn},
                        success: function (respObj) {     
                          if(respObj.success == true)
                          {
                           toastr.success(respObj.message,{
                            "showDuration":"1000",
                            "timeOut" : "800",
                            "iconClass" : "toast toast-success",
                            "closeButton": true,
                            "positionClass": "toast-top-right"
                             });
                            setTimeout(function() {
                                location.reload();
                                 window.location.href = "<?=route('category.index')?>";
                            }, 2000);                                
                            }
                        }
                    });
                }
                else
                {
                   toastr.error('Please select at least one contact',{
                    "showDuration":"1000",
                    "timeOut" : "800",
                    "iconClass" : "toast toast-success",
                    "closeButton": true,
                    "positionClass": "toast-top-right"
                   });
                }
             });
  

              // send latter
             //  $('#lattera').click(function(){
             //    var total_user = $('input:checkbox.single_check:checked').length;
             //    // var radioValue = $("input[name='mail_type']:checked").val();
             //    var id = "<?= $id ?>";
             //    var selectedVal = new Array();
             //    $('input:checkbox.single_check').each(function () {
             //           if($(this).is(':checked'))
             //           {                         
             //               selectedVal.push(this.value);       
             //           }
             //       });
              
             //    if($('input:checkbox.single_check').is(':checked')){
                     
             //          var selected_id = selectedVal;                          
             //          var token = "<?=csrf_token()?>";  
             //          $.ajax({
             //            url:"<?=route('sendmail.latter');?>",
             //            type:'post',
             //            dataType:'json',
             //            data: {_token:token,selected_id:selected_id,id:id},
             //            success: function (respObj) {     
             //              if(respObj.success == true)
             //              {
             //               toastr.success(respObj.message,{
             //                "showDuration":"1000",
             //                "timeOut" : "800",
             //                "iconClass" : "toast toast-success",
             //                "closeButton": true,
             //                "positionClass": "toast-top-right"
             //                 });

             //                setTimeout(function() {
             //                    location.reload();
             //                     window.location.href = "<?=route('category.index')?>";
             //                }, 2000);                                    
             //              }
             //            }
             //        });
             //    }
             //    else
             //    {
             //       toastr.error('Please select at least one contact',{
             //        "showDuration":"1000",
             //        "timeOut" : "800",
             //        "iconClass" : "toast toast-success",
             //        "closeButton": true,
             //        "positionClass": "toast-top-right"
             //         });
             //    }
             // });
         });
         
         $("#preview").click(function(){
             var status = $('input[name=mail_type]:checked').val();
             var message = $('#template_content').val();
             var data = $('#message_data').html(message);
         });
         
         $('.custom_change').change(function(){
             var custom_val = $(this).val();
             if(custom_val != 'defaul')
             {
                $('.enter_record').show();                 
             }
             else
             {
                $('.enter_record').hide();
             }
         });


            // $(document).ready(function(){
            //     $(".single_check").click(function () {
            //       var user_id = $(this).val();
            //        alert($(this).val());
            //     });
            //  });
         
      </script>
      <script type="text/javascript">
            $('#timepicker1').timepicker();
        </script>
   </body>
</html>