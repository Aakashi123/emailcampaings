<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="/plugins/ckeditor/ckeditor.js"></script>
  <script  src="/js/index.js"></script>
  <script type="text/javascript">
  CKEDITOR.replace( 'messageArea',
  {
    customConfig : 'config.js',
    toolbar : 'simple'

  })

  // CKEDITOR.instances['TEXTAREA_NAME'].setReadOnly(false);

</script> 

  <style type="text/css">
  	
   table {
    border-spacing: 0;
    border-collapse: collapse;
    text-align: center;
    margin-left: 48px;
}
  	p {
    margin: 0 0 10px;
    text-align: center;
}

.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
}
input{
    margin: 10px;
    font: inherit;
    color: inherit;
    margin-right: 0px;
}

button{
    margin: 10px;
    font: inherit;
    color: inherit;
    margin-right: 0px;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    margin-top: 25px;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
  </style>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#myForm:span").prop("disabled", true);
    });
</script>
</head>
<body> 
<div class="container">
  <h2>Create Campaign</h2>
  <div class="panel panel-default">
    <div class="panel-heading">Create Campaign...</div>
    <div class="panel-body">
     <form method="POST" id="myForm" action="{{ route('createtemplate.store') }}" enctype="multipart/form-data">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" name="id" value="{{ $id }}">
      <textarea id="messageArea" name="messageArea" rows="7" class="form-control ckeditor" placeholder="Write your message.." value ="{{$content}}" >{{$content}}</textarea>   

       <!-- <button type="button" class="btn btn-info " id = "preview" data-toggle="modal" data-target="#myModal">Preview</button> -->
  
       <button type="submit" class="btn btn-info pull-right" id="save_data" name="save" value="Save">Save</button>
       <!-- <input  type="submit" class="btn btn-primary pull-right" name="save" value="Send"> -->
       @if($type != null)
          <a href="<?=route('employee.details',['id'=>$id]) ?>" id = "next" class="btn btn-primary pull-right">Next</a> 
       @else
          <a href="javascript::void(0)"><button class="btn btn-primary pull-right" disabled="">Next</button></a> 
       @endif
            
     </form>
    </div>
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Preview</h4>
        </div>
        <div class="modal-body" id="message_data">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="previous btn btn-info pull-left">&lt; Previous</button>
          <button type="button" class="next btn btn-info pull-right">Next &gt;</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>