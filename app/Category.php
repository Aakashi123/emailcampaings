<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
	protected $fillable = ['id', 'title', 'slug', 'image'];


	public function category()
    {
    	return $this->belongsTo(Subcategory::class);
    }

    public function Conatctdetail()
    {
    	return $this->hasMany(ConatctDetail::class);
    }
	
}
