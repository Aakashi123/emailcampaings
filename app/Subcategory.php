<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    
    protected $table = 'subcategories';
	protected $fillable = ['id','cate_id','subcate_name', 'slug','image','content'];


	public function subcategory()
    {
    	return $this->hasMany(Category::class);
    }


    public function ConatctDetail()
    {
    	return $this->hasMany(ConatctDetail::class);
    }
}
