<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use Illuminate\Support\Facades\Storage;
use Mail,View;
use App\ConatctDetail;
use App\UserTemplate;
use App\Jobs\SendMailJob;
use File;
use App\EmailInfo;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $cate = Category::all()->toArray();  
        
        return view('category', compact('cate'));
      
    }

    public function testEmail(Request $request){

         $id = $request->get('id');

         $template = Subcategory::select('content')->where('id',$id)->get()->toArray();
         
         return view('contact_detail', compact('template'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        $slug = $request->get('slug');
        $data = Category::where('slug',$slug)->first();

        $query = Subcategory::where('cate_id',$data['id'])->get();
        
        return view('subcategory',compact('query'));
    }
    public function createtemplate(Request $request,$id){ 

        return view('createcampaign',compact('id')); 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
      
        $data = $request->all();
        
        $temp = UserTemplate::firstOrNew(['tamplate_id'=> $data['id']]);
        
        $temp->tamplate_id = $data['id'];
        $temp->content  = $data['messageArea'];
        $temp->save();
        
        return redirect()->route('createtemplate.edit',['id'=>$data['id'],'type'=>'updated']);
    }
    public function details(Request $request){

        $id = $request->get('id');

        $detail = ConatctDetail::all()->toArray();
        
        $content = UserTemplate::where('tamplate_id', $id)->value('content');

        return view('contact_detail',compact('detail','id','content'));
    }
    public function editTemplate(Request $request){
        
        $data = $request->all();
        $id = $request->get('id');
        
        $type = '';

        $record = Subcategory::find($id)->toArray();

        if(array_key_exists('type', $data))
        {
            $type = 'updated';  

            $record = UserTemplate::firstOrNew(['tamplate_id'=> $id]);

        }
        $content = $record['content'];  
        
        return view('createcampaign',compact('content','id','type'));
    }

    public function sendEmail(Request $request){

        $contact = ConatctDetail::where('id', $request->get('selected_id'))->first();

        $template = UserTemplate::where('tamplate_id', $request->get('id'))->value('content');

        if ($contact['email_id'] != null) {

            if ($request['radioValue'] == 'personal') 
            {
                $send_message = $this->dispatch(new SendMailJob(['name' => $contact->name,'type'=>$request['radioValue'],'content'=>$template], $contact['email_id']));

                return response()->json(array('success' => true, 'message' => 'Message sent sucessfully to '.$contact['name']), 200);
            }
            else
            {
                $send_message = $this->dispatch(new SendMailJob(['name' => $contact->name,'type'=>$request['radioValue'],'content'=>$template], $contact['email_id']));

                return response()->json(array('success' => true, 'message' => 'Message sent sucessfully to '.$contact['name']), 200);
            }

        } else 
        {
                    
            return response()->json(array('success' => false, 'message' => 'No email available for ' . $contact['name'] . ' to send message'), 200);
        }
    }

    public function setOption(Request $request)
    {
        $info = $request->all();
        echo "<pre>";
        print_r($info);
        exit();
        $contact_id =$request['selected_id'];

        foreach ($contact_id as $key => $value) {
            $emailinfo = new EmailInfo;
            $emailinfo->contact_id = $value;
            $emailinfo->type = $info['radioValue'];
            $emailinfo->template_id = $info['id'];

            $emailinfo->save();
        }
        $emailinfo_data = EmailInfo::orderBy('updated_at','desc')->first();

       return response()->json(array('success'=>true,'message'=>'update'),200);
        // return view('email_detail',compact('emailinfo_data'));
           

    }

    public function sendMailLatter(Request $request)
    {
        echo "<pre>";
        print_r($request->all());
        exit();
    }
}
