<?php

namespace App\Jobs;

use App\Mail\SendMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendMailJob
{
    use InteractsWithQueue, SerializesModels;

    protected $email_data;
    protected $email_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_data, $email_id) {

        $this->email_data = $email_data;
        $this->email_id = $email_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        $email_data = $this->email_data;
        // dd($email_data);
        $email_id = $this->email_id;
        $send_mail = new SendMessage($email_data);
        Mail::to($email_id)->queue($send_mail);
    }
}
