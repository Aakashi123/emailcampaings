<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConatctDetail extends Model
{
    protected $table = 'contact_details';
	protected $fillable = ['id','subcategory_id','cate_id','name','number','email_id'];

	public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
    	return $this->belongsTo(Subcategory::class);
    }
	
}
