<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTemplate extends Model
{
    protected $table = 'users_template';
	protected $fillable = ['id','tamplate_id','content'];

}
