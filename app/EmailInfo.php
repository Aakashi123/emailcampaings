<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailInfo extends Model
{
    protected $table = 'email_info';
	protected $fillable = ['id','contact_id','type','status','template_id'];
}
