<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $email_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_data) {

        $this->email_data = $email_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        $email_data = $this->email_data;
            // dd($email_data);
        $content = $email_data['content'];
        if ($email_data['type'] == 'personal') {
            
            $content = str_replace('Dear',$email_data['name'], $content);
            
            return $this->subject('Birthday celebration')
                ->view('email.email', compact('content'));
        }
           
            return $this->subject('Birthday celebration')
                ->view('email.email',compact('content'));

    }
}


